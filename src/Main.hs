{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

import Web.Scotty
import qualified Web.Scotty as S
import Text.Blaze.Html.Renderer.Text
import Text.Blaze.Html5
import qualified Text.Blaze.Html5 as B
import Text.Blaze.Html5.Attributes
import Text.Blaze.Internal (stringValue)
import HaskellExpr.Eval (evalSource)
import Data.String (fromString)
import Data.List (intercalate)

interpPath = "/lang"
interpAttr = stringValue interpPath
interpRoute = fromString interpPath

interpPage env = do
        B.html $ do
            B.title "Little interpreter"
            B.body $ do
                h1 "Lil' interpreter"
                B.form ! action interpAttr ! method "post" $ do
                    textarea ! name "code" $ ""
                    input ! type_ "hidden" ! name "env" ! value (stringValue env)
                    input ! type_ "submit"
                code (toHtml env)
                h2 "Examples"
                ul $ mapM_ (li . toHtml) ([
                         "a = 1",
                         "b = 2",
                         "c = a + b",
                         "%add(lhs, rhs) lhs + rhs"
                     ] :: [String])
                a ! href "http://github.com/Vermeille/HaskellExpr" $
                    "See it on GitHub!"

main = scotty 8181 $ do
        get "/" $ do
            S.html . renderHtml $ do
                h1 "Welcome"
                ul $ do
                    li $ a ! href "/vm-proto" $ "Play with my VM's proto!"
                    li $ a ! href "/lang" $ "Use my little interpreter"
                p "All computations are done server side in Haskell. No JS."

        get interpRoute $ do
            S.html . renderHtml $ do
                interpPage "fromList []"
        post interpRoute $ do
            code :: String <- S.param "code"
            env :: String <- S.param "env"
            let (res, (newEnv, _), _) = evalSource code env
            S.html . renderHtml $ do
                interpPage $ show newEnv
                p $ toHtml $ show $ res

